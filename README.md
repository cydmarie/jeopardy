JEOPARDY

For this project, I created a game of Jeopardy. Once a user comes to the site, they can select a point value from the categories shown. They will receive a question and 4 multiple choice answers to choose from. They will then see their score rise (by the given point value) on the scoreboard. Once all questions have been submitted the game is over and you will receive your final score.  

Deployed app: http://jeopardy.cydmarie.bitballoon.com/

Technologies Used:
HTML5, CSS, JavaScript, jQuery, Bootstrap

Features: Modals, scoreboard, right/wrong answer notifications and final score tally.

Future Development:
I'd like to add daily double questions, a Round 2, music when first logging into the site and also to keep time while answering question.  I'd also do a lot more styling.